package com.aiotouch.allbanks.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.aiotouch.allbanks.model.Bank;
import com.aiotouch.allbanks.repository.BankRepository;
import com.aiotouch.allbanks.service.BankService;

@Service
public class BankServiceImpl implements BankService {

	private BankRepository bankRepository;
	
	public BankServiceImpl(BankRepository bankRepository) {
		super();
		this.bankRepository = bankRepository;
	}

	@Override
	public Bank save(Bank bank) {
		return bankRepository.save(bank);
	}

	@Override
	public List<Bank> getAllBanks() {
		return bankRepository.findAll();
	}

	@Override
	public Bank getBanksByCountryCode(String countryCode) {
		return bankRepository.findByCountryCode(countryCode);
	}

	@Override
	public Bank getBanksByCountryName(String name) {
		return bankRepository.findByCountryName(name);
	}

}
