package com.aiotouch.allbanks.service;

import java.util.List;

import com.aiotouch.allbanks.model.Bank;

public interface BankService {

	Bank save(Bank bank);
	
	List<Bank> getAllBanks();
	
	Bank getBanksByCountryCode(String countryCode);
	
	Bank getBanksByCountryName(String name);
	
}
