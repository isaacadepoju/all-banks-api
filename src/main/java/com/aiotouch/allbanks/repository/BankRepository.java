package com.aiotouch.allbanks.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.aiotouch.allbanks.model.Bank;

public interface BankRepository extends JpaRepository<Bank, Long> {

	Bank findByCountryCode(String countryCode);

	Bank findByCountryName(String name);

}
